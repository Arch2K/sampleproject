using System;
using System.Collections.Generic;
using Sample.Models;

namespace Sample.Repositories
{
    // Repository interface
    public interface ISampleRepository
    {
        // Some CRUD
        IEnumerable<SampleDomainModel> GetObjects(string name);
        void CreateObj(SampleDomainModel model);

        bool DeleteObj(Guid id, string customer, out SampleDomainModel model);
    }
}